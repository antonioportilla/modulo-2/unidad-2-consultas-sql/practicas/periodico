<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etiqueta".
 *
 * @property int $ide
 * @property string $nombre
 *
 * @property Ne[] $nes
 */
class Etiqueta extends \yii\db\ActiveRecord
{
    public $noticias;
    public $autores;
    public $titulo;
    public $texto;
    public $idn;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'etiqueta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 127],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ide' => 'Ide',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNes()
    {
        return $this->hasMany(Ne::className(), ['ide' => 'ide']);
    }
}
