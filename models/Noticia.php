<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticia".
 *
 * @property int $idn
 * @property string $titulo
 * @property string $texto
 * @property string $fecha
 * @property int $ida
 *
 * @property Autor $a
 * @property Picture[] $pictures
 */
class Noticia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ida'], 'integer'],
            [['titulo'], 'string', 'max' => 63],
            [['texto'], 'string', 'max' => 255],
            [['fecha'], 'string', 'max' => 30],
            [['ida'], 'exist', 'skipOnError' => true, 'targetClass' => Autor::className(), 'targetAttribute' => ['ida' => 'ida']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idn' => 'Idn',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'ida' => 'Ida',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getA()
    {
        return $this->hasOne(Autor::className(), ['ida' => 'ida']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPictures()
    {
        return $this->hasMany(Picture::className(), ['idn' => 'idn']);
    }
    
    public function getNes()
    {
        return $this->hasMany(Etiqueta::className(), ['ide' => 'ide'])->viaTable('ne', ['idn' => 'idn']);;
    }
    
   
         /* metodo nuevo, el listado de fecha sale en castellano */ 
    public function afterFind(){
        parent::afterFind();
        // tendria el año de un registro de alquileres automaticamente de forma dinamica
        // $this->campoyear=Yii::$app->formatter->asDate($this->fecha, 'php:Y');
        //$this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');
        //$this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)-> format("Y/m/d");
    }
    
    /* antes de grabar, el listado de fecha guarda en castellano*/
    
    /*public function beforeSave($insert){
        parent::beforeSave($insert);
      
        //$this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)-> format("Y/m/d");
    } */
     
    
    
}
