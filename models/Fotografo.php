<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotografo".
 *
 * @property int $idf
 * @property string $nombre
 * @property string $alias
 *
 * @property Email[] $emails
 * @property Picture[] $pictures
 */
class Fotografo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotografo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'alias'], 'string', 'max' => 63],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idf' => 'Idf',
            'nombre' => 'Nombre',
            'alias' => 'Alias',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmails()
    {
        return $this->hasMany(Email::className(), ['idf' => 'idf']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPictures()
    {
        return $this->hasMany(Picture::className(), ['idf' => 'idf']);
    }
    
    public function getNoticias(){
        return $this->hasMany(Noticia::className(),['idn'=>'idn'])->viaTable('picture', ['idf'=>'idf']);
    }
    
    public function getAutores(){
        return $this->hasMany(Autor::className(),['ida'=>'ida'])->via('noticias');
    }
}
