﻿DROP DATABASE IF EXISTS periodico;
CREATE  DATABASE  periodico;

USE periodico;

DROP TABLE IF EXISTS autor;
CREATE TABLE autor (
  ida int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(63),
  alias varchar(63),
  email varchar(30)
  );



DROP TABLE IF EXISTS fotografo;
CREATE TABLE fotografo (
  idf int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(63),
  alias varchar(63)
   );




DROP TABLE IF EXISTS email;
CREATE TABLE email(
  ide int AUTO_INCREMENT PRIMARY KEY,
  email varchar(127),
  idf int,
  FOREIGN KEY (idf) REFERENCES fotografo(idf)
  );




DROP TABLE IF EXISTS noticia;
CREATE TABLE noticia (
  idn int AUTO_INCREMENT PRIMARY KEY,
  titulo varchar(63),
  texto varchar(255),
  fecha varchar(30),
  ida int,
  FOREIGN KEY (ida) REFERENCES autor(ida)
  );
DROP TABLE IF EXISTS etiqueta;
CREATE TABLE etiqueta (
  ide int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(127)
  );
DROP TABLE IF EXISTS ne;
CREATE TABLE ne (
  idne int AUTO_INCREMENT PRIMARY KEY,
  ide int,
  idn int,
  FOREIGN KEY (ide) REFERENCES etiqueta(ide),
  FOREIGN KEY (idn) REFERENCES noticia(idn)
  );
DROP TABLE IF EXISTS picture;
CREATE TABLE picture(
  idp int AUTO_INCREMENT PRIMARY KEY,
  foto varchar(255),
  idf int,
  idn int,
  FOREIGN KEY (idf) REFERENCES fotografo(idf),
  FOREIGN KEY (idn) REFERENCES noticia(idn)
  );

