<?php

namespace app\controllers;

use Yii;
use app\models\Etiqueta;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\db\Query;
use app\models\Ne;
use app\models\Noticia;
use yii\filters\AccessControl;


/**
 * EtiquetaController implements the CRUD actions for Etiqueta model.
 */
class EtiquetaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => ['listar','index','list','view','create','update','delete','noticiasetiqueta'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    
        /**
     * Lists all Etiqueta models.
     * @return mixed
     */
    public function actionList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Etiqueta::find(),
        ]);

        return $this->render('listview', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Etiqueta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Etiqueta::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Etiqueta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Etiqueta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Etiqueta();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ide]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Etiqueta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ide]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Etiqueta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Etiqueta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Etiqueta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Etiqueta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    /**
     * Lists all Etiqueta models.
     * @return mixed
     */
    public function actionListar()
    {
       // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT distinct COUNT(*) FROM etiqueta INNER JOIN ne USING(ide) INNER JOIN noticia USING(idn) GROUP BY
                nombre")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT ide, nombre, COUNT(*) noticias, COUNT(DISTINCT ida) autores 
                FROM etiqueta INNER JOIN ne USING(ide) INNER JOIN noticia USING(idn) GROUP BY
                nombre,ide',
            ]);
        
       /* $dataProvider = new ActiveDataProvider([
            'query' => Etiqueta::find(),
        ]);*/
 
       return $this->render('etiqueta', [
            'dataProvider' => $dataProvider,
            "campos"=>['nombre', 'noticias', 'autores', 'ide'], 
        ]);
       
    }
    public function actionNoticiasetiqueta($id){
        
        // mediante active record
        /*SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal LEFT JOIN
        puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL;*/
        $c1 = Ne::find()
               ->distinct() 
               ->innerJoin('noticia', 'ne.idn = noticia.idn')
               ->where(['ide'=>$id]);
       
        $dataProvider = new ActiveDataProvider([
            'query' => $c1,
          ]);
    
        return $this->render("listview", [
            "dataProvider"=>$dataProvider,
           "campos"=>['titulo, texto'], 
           
        ]);
        
    }
        
        
     
    
    
    
        
        
      
    
      
      
      
      
    }
