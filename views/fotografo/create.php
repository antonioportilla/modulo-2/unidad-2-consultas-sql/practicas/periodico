<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fotografo */

$this->title = 'Create Fotografo';
$this->params['breadcrumbs'][] = ['label' => 'Fotografos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fotografo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
