<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Etiqueta */

$this->title = 'Update Etiqueta: ' . $model->ide;
$this->params['breadcrumbs'][] = ['label' => 'Etiquetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ide, 'url' => ['view', 'id' => $model->ide]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="etiqueta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
