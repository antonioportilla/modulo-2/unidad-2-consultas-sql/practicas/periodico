<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Noticia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="noticia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'texto')->textInput(['maxlength' => true]) ?>
    
    <?php
    echo $form->field($model,'fecha')->widget(DatePicker::className(),[ //opcion 2 de sacar las etiquetas de los atributos
        'options' => ['placeholder' => 'Introduce la fecha de la Noticia'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'autoclose'=>true,
           //'format'=>'yyyy/mm/dd',     // opcion de guardar con fecha formato original*/ 
           'format'=>'dd/mm/yyyy', 
        ]
    ]);  
    ?>
    
    

    

    <?= $form->field($model, 'ida')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
