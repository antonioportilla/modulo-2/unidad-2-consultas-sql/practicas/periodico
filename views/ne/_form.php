<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ne */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ne-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ide')->textInput() ?>

    <?= $form->field($model, 'idn')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
