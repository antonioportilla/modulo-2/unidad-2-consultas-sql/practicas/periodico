<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ne */

$this->title = 'Create Ne';
$this->params['breadcrumbs'][] = ['label' => 'Nes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ne-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
